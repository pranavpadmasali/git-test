import React, { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";

// Config object with the API URL
const config = {
  apiUrl: "http://13.234.136.118:8000", // Replace with your backend API URL
};

function App() {
  const [employeeData, setEmployeeData] = useState([]);
  const [newEmployee, setNewEmployee] = useState({ name: "", department: "" });

  useEffect(() => {
    // Fetch employee data from the backend API
    axios
      .get(`${config.apiUrl}/getemployee`)
      .then((response) => {
        setEmployeeData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setNewEmployee({ ...newEmployee, [name]: value });
  };

  const addEmployee = () => {
    axios
      .post(`${config.apiUrl}/addemployee`, newEmployee)
      .then((response) => {
        console.log("Employee added successfully:", response.data.message);
        // Fetch updated employee data
        axios
          .get(`${config.apiUrl}/getemployee`)
          .then((response) => {
            setEmployeeData(response.data);
          })
          .catch((error) => {
            console.error("Error fetching data:", error);
          });
      })
      .catch((error) => {
        console.error("Error adding employee:", error.response.data.error);
      });
  };

  return (
    <div className="App">
      <h1>Employee List</h1>
      <ul>
        {employeeData.map((employee) => (
          <li key={employee.id}>
            {employee.name} - {employee.department}
          </li>
        ))}
      </ul>

      <h2>Add Employee</h2>
      <div>
        <label>Name:</label>
        <input
          type="text"
          name="name"
          value={newEmployee.name}
          onChange={handleInputChange}
        />
      </div>
      <div>
        <label>Department:</label>
        <input
          type="text"
          name="department"
          value={newEmployee.department}
          onChange={handleInputChange}
        />
      </div>
      <button onClick={addEmployee}>Add Employee</button>
    </div>
  );
}

export default App;

