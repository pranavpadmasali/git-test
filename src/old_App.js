import React, { useState, useEffect } from "react";
import axios from "axios";

// Config object with the API URL
const config = {
  apiUrl: "http://13.234.136.118:8000", // Replace with your backend API URL
};

function App() {
  const [employeeData, setEmployeeData] = useState([]);

  useEffect(() => {
    // Fetch employee data from the backend API
    axios
      .get(`${config.apiUrl}/getemployee`)
      .then((response) => {
        setEmployeeData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  return (
    <div className="App">
      <h1>Employee List</h1>
      <ul>
        {employeeData.map((employee) => (
          <li key={employee.id}>
            {employee.name} - {employee.department}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default App;

